module "network" {
  source      = "../moduels/network"
  cidr_block  = "10.0.0.0/16"
  az          = ["ap-southeast-1a", "ap-southeast-1b"]
  nat-subnet  = ["10.0.1.0/24", "10.0.2.0/24"]
  app-subnet  = ["10.0.3.0/24", "10.0.4.0/24"]
  data-subnet = ["10.0.5.0/24", "10.0.6.0/24"]
}

module "database" {
  source         = "../moduels/database"
  storage        = 10
  engine         = "mysql"
  engine_version = "5.7"
  instance_class = "db.t3.small"
  db_name        = "Population"
  username       = "admin"
  password       = "testingrdscluster"
  depends_on     = [module.network]
}

module "compute" {
  source         = "../moduels/compute"
  ami            = "ami-0ff89c4ce7de192ea"
  instance_type  = "t2.micro"
  aws_key_id     = ""
  aws_access_key = ""
  aws_region     = "ap-southeast-1"
  depends_on     = [module.network, module.database]
}

output "db_endpont" {
  value = module.compute.db_endpont
}

output "key_ssh" {
  value     = module.compute.private-key
  sensitive = true
}
