data "aws_subnet" "data-subnet" {
  count = 2
  filter {
    name   = "tag:Name"
    values = ["data-subnet-${count.index + 1}"]
  }
}

data "aws_security_group" "db-sg" {
  filter {
    name   = "tag:Name"
    values = ["db-sg"]
  }
}
