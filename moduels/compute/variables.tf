variable "ami" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "aws_key_id" {
  type = string
}

variable "aws_access_key" {
  type = string
}

variable "aws_region" {
  type = string
}