output "db_endpont" {
  value = data.aws_db_instance.db-endpoint.address
}

output "private-key" {
  value = tls_private_key.ssh.private_key_pem
}