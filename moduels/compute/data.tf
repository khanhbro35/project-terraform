data "aws_db_instance" "db-endpoint" {
  db_instance_identifier = "data"
}

data "aws_subnet" "app-subnet" {
  count = 2
  filter {
    name   = "tag:Name"
    values = ["app-subnet-${count.index + 1}"]
  }
}

data "aws_subnet" "nat-subnet" {
  count = 2
  filter {
    name   = "tag:Name"
    values = ["nat-subnet-${count.index + 1}"]
  }
}

data "aws_security_group" "ec2-sg" {
  filter {
    name   = "tag:Name"
    values = ["ec2-sg"]
  }
}

data "aws_security_group" "bastion-host" {
  filter {
    name   = "tag:Name"
    values = ["bastion_host"]
  }
}

data "aws_security_group" "lb-sg" {
  filter {
    name   = "tag:Name"
    values = ["lb-sg"]
  }
}

data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Name"
    values = ["vpc"]
  }
}