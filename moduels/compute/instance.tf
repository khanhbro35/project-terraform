resource "aws_instance" "bastion_host" {
  ami                         = var.ami
  instance_type               = var.instance_type
  subnet_id                   = data.aws_subnet.nat-subnet[0].id
  key_name                    = aws_key_pair.ssh.key_name
  associate_public_ip_address = true
  security_groups             = [data.aws_security_group.bastion-host.id]
  tags = {
    "Name" = "bastion_host"
  }
}

resource "aws_instance" "app" {
  ami             = var.ami
  instance_type   = var.instance_type
  subnet_id       = data.aws_subnet.app-subnet[count.index].id
  security_groups = [data.aws_security_group.ec2-sg.id]
  key_name        = aws_key_pair.ssh.key_name
  user_data       = <<EOF
    #!/bin/bash
    sudo yum update -y
    sudo yum install -y python3
    export AWS_ACCESS_KEY_ID=${var.aws_key_id}
    export AWS_SECRET_ACCESS_KEY=${var.aws_access_key}
    export AWS_DEFAULT_REGION=${var.aws_region}
    aws s3 cp s3://us-west-2-aws-training/awsu-spl/SPL-TF-200-NWCDVW-1/1.0.6.prod/scripts/vpcapp.zip .
    unzip vpcapp.zip
    cd vpcapp
    pip3 install -r requirements.txt
    export DATABASE_HOST=${data.aws_db_instance.db-endpoint.address}
    export DATABASE_USER=admin
    export DATABASE_PASSWORD=testingrdscluster
    export DATABASE_DB_NAME=Population
    cd loaddatabase
    python3 database_populate.py
    cd ..
    python3 application.py
  EOF
  count           = 2
  tags = {
    "Name" = "app-${count.index + 1}"
  }
}


resource "aws_lb_target_group" "tg" {
  name     = "tg"
  port     = 8443
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.vpc.id
}

resource "aws_lb_target_group_attachment" "attachment_tg" {
  target_group_arn = aws_lb_target_group.tg.arn
  target_id        = aws_instance.app[count.index].id
  port             = 8443
  count            = 2
}

resource "aws_lb" "lb" {
  name               = "lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [data.aws_security_group.lb-sg.id]
  subnets            = [for subnet in data.aws_subnet.nat-subnet : subnet.id]
}

resource "aws_lb_listener" "ls" {
  load_balancer_arn = aws_lb.lb.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg.arn
  }
}

resource "aws_lb_listener_rule" "lsr" {
  listener_arn = aws_lb_listener.ls.arn
  priority     = 100
  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg.arn
  }
  condition {
    path_pattern {
      values = ["/"]
    }
  }
}