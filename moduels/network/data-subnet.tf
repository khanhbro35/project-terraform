resource "aws_subnet" "data-subnet" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.data-subnet[count.index]
  availability_zone = var.az[count.index]
  count             = length(var.data-subnet)
  tags = {
    "Name" = "data-subnet-${count.index + 1}"
  }
}