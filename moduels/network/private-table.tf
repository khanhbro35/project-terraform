resource "aws_route_table" "private-table" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat.id
  }
}

resource "aws_route_table_association" "private-table-association" {
  route_table_id = aws_route_table.private-table.id
  subnet_id      = aws_subnet.app-subnet[count.index].id
  count          = length(var.app-subnet)
}