variable "cidr_block" {
  type = string
}

variable "az" {
  type = list(any)
}

variable "nat-subnet" {
  type = list(any)
}

variable "app-subnet" {
  type = list(any)
}

variable "data-subnet" {
  type = list(any)
}

