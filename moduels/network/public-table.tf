resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc.id
}

resource "aws_eip" "eip" {
  vpc = true
}

resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.eip.id
  subnet_id     = aws_subnet.nat-subnet[0].id
  # depends_on    = [aws_internet_gateway.gw]
}

resource "aws_route_table" "public-table" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
}

resource "aws_route_table_association" "public-table-association" {
  subnet_id      = aws_subnet.nat-subnet[count.index].id
  route_table_id = aws_route_table.public-table.id
  count          = length(var.nat-subnet)
}