resource "aws_subnet" "app-subnet" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.app-subnet[count.index]
  availability_zone = var.az[count.index]
  count             = length(var.app-subnet)
  tags = {
    "Name" = "app-subnet-${count.index + 1}"
  }
}