resource "aws_subnet" "nat-subnet" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.nat-subnet[count.index]
  availability_zone = var.az[count.index]
  count             = length(var.nat-subnet)
  tags = {
    "Name" = "nat-subnet-${count.index + 1}"
  }
}
