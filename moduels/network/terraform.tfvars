cidr_block  = "10.0.0.0/16"
az          = ["ap-southeast-1a", "ap-southeast-1b"]
nat-subnet  = ["10.0.1.0/24", "10.0.2.0/24"]
app-subnet  = ["10.0.3.0/24", "10.0.4.0/24"]
data-subnet = ["10.0.5.0/24", "10.0.6.0/24"]